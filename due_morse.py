from arduino import Arduino
import sys
import time

b = Arduino('/dev/ttyACM0')
pin = 13
b.output([pin])

CODE = {'A': '.-',     'B': '-...',   'C': '-.-.', 
        'D': '-..',    'E': '.',      'F': '..-.',
        'G': '--.',    'H': '....',   'I': '..',
        'J': '.---',   'K': '-.-',    'L': '.-..',
        'M': '--',     'N': '-.',     'O': '---',
        'P': '.--.',   'Q': '--.-',   'R': '.-.',
        'S': '...',    'T': '-',      'U': '..-',
        'V': '...-',   'W': '.--',    'X': '-..-',
        'Y': '-.--',   'Z': '--..',
        
        '0': '-----',  '1': '.----',  '2': '..---',
        '3': '...--',  '4': '....-',  '5': '.....',
        '6': '-....',  '7': '--...',  '8': '---..',
        '9': '----.' 
        }
        
UNIT_BASE = .33
UNIT_LETTER = 3 * UNIT_BASE
UNIT_DASH = 3 * UNIT_BASE
UNIT_SPACE = 7 * UNIT_BASE

b.setLow(pin)

def verify(string):
    keys = CODE.keys()
    for char in string:
        if char.upper() not in keys and char != ' ':
            sys.exit(u'Error! The charcter {} cannot be translated to Morse Code'.format(char))
    return string

def main():
    msg = verify(raw_input(u'MESSAGE: '))
    for char in msg:
        if char == ' ':
            print ' '*7+'\n',
            time.sleep(UNIT_SPACE)
        else:
            print CODE[char.upper()]
            for c in CODE[char.upper()]:
                if c == '.':
                    b.setHigh(pin)
                    time.sleep(UNIT_BASE)
                    b.setLow(pin)
                else:
                    b.setHigh(pin)
                    time.sleep(UNIT_DASH)
                    b.setLow(pin)
        time.sleep(UNIT_LETTER)

if __name__ == '__main__':
    main()
